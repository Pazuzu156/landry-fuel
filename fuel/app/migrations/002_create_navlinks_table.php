<?php

namespace Fuel\Migrations;

class Create_navlinks_table
{
	public function up()
	{
		\DBUtil::create_table('navlinks_table', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'parent' => array(
				'constraint' => 11,
				'type' => 'int'
			),
			'has_child' => array(
				'type' => 'boolean'
			),
			'divider' => array(
				'type' => 'boolean'
			),
			'newtab' => array(
				'type' => 'boolean'
			),
			'content' => array(
				'constraint' => 255,
				'type' => 'varchar'
			),
			'url' => array(
				'constraint' => 255,
				'type' => 'varchar'
			),
			'created_at' => array(
				'type' => 'datetime'
			),
			'updated_at' => array(
				'type' => 'datetime'
			)
		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('navlinks_table');
	}
}