<?php

class Controller_Home extends Controller_Base
{
	/**
	 * Index method loads in home page
	 */
	public function action_index()
	{
		return $this->render('home/index');
	}

	/**
	 * Presenter method loads presenter.
	 *
	 * Presenters are a bit reworked from standard FuelPHP presenters
	 * Presenters are now "abstract" views following standard presenter workflow
	 *
	 * To load a presenter, pass in the presenter's view AND the base view
	 * that will be used to display the rendered presenter
	 */
	public function action_presenter()
	{
		if(Request::active()->get_method() == 'GET')
			return Response::forge($this->presenter('home/presenters/form', 'home/form'));
		else
		{
			$text = Input::post('text');
			if(empty($text))
			{
				Session::set_flash('emsg', 'The text field must not be empty!');
				return Response::redirect('/testpage');
			}
			else
				return Response::redirect('/testpage/'.Input::post('text'));
		}
	}

	/**
	 * This action loads a 404 error page if a request isn't found
	 */
	public function action_404()
	{
		return $this->render('errors/404');
	}
}
