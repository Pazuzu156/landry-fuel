<?php

class Controller_Admin extends Controller_Template
{
  public function action_index()
  {
    if(!Auth::check())
      return Response::redirect('admin/login');
    else
      return $this->render('admin/index');
  }

  public function action_get_login()
  {
    $fs = Fieldset::forge();
    $form = $fs->form();
    $this->template->content = View::forge('admin/login');
    // $this->template->set('content', $form->build(), false);
  }
}
