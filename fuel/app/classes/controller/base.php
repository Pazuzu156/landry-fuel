<?php

class Controller_Base extends Controller
{
	/**
	 * Allows for rendering a view within a layout
	 *
	 * @param string $view
	 * @param array $options
	 * @return View
	 */
	public function render($view, $options=array())
	{
		$this->collect_data($options); // pass by reference ;)

		$options['view'] = View::forge($view, $options);

		$v = View::forge('layout/main', $options);

		return $v->render();
	}

	/**
	 * Allows for rendering a presenter within a layout
	 *
	 * @param string $presenter
	 * @param string $baseView
	 * @param array $options
	 * @return View
	 */
	public function presenter($presenter, $baseView, $options=array())
	{
		// Create presenter
		$options['abstract'] = Presenter::forge($presenter);

		// Just call our render method, the presenter is
		// set in the options array to be passed into the view
		return $this->render($baseView, $options);
	}

	/**
	 * Collects data for master layout
	 *
	 * @param array &$options
	 * @return array
	 */
	private function collect_data(&$options)
	{
		$nb = Model_Navlink::find('all');

		$options['navbar'] = View::forge('layout/navbar', array('navlinks' => $nb));

		$options['assets'] = array(
			'css' => array(
				'bootstrap.min.css',
				'theme.min.css',
				'animate.min.css',
				'site.css'
			),
			'js'  => array(
				'global.min.js',
			)
		);

		// Temporary. Will be replaced for
		// title in db once implemented
		$lang = \Lang::load('us.yml');
		$options['title'] = $lang['headings']['title'];
		$options['headers'] = $lang['headings'];

		return $options;
	}
}
