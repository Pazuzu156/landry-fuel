<?php

class Model_Navlink extends Orm\Model
{
	protected static $_properties = array('id', 'content', 'parent', 'has_child', 'url', 'newtab', 'divider');

	protected static $_table_name = 'navlinks_table';
}