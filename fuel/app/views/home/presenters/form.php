<?php if(!empty($presenter)): ?>
<div class="alert alert-success alert-dismissable" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hiden="true">&times;</span></button>
	<strong>Your Text: </strong><?php echo $presenter; ?>
</div>
<?php endif; ?>

<?php echo (empty($presenter)) ? '' : Html::anchor('/testpage', '&laquo; Back'); ?>
