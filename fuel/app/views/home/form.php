<h2><?php echo $headers['pres_page_head']; ?></h2>
<?php echo $abstract; ?>
<?php
$emsg = Session::get_flash('emsg');
if(!empty($emsg)):
?>
<div class="alert alert-danger alert-dismissable" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hiden="true">&times;</span></button>
	<strong>Your Text: </strong><?php echo $emsg; ?>
</div>
<?php endif; ?>
<p>Type some text into the form below!</p>
<?php

echo Form::open();
echo Form::label('Text:', 'data');
echo Form::input('text', '', array('class' => 'form-control')) . "<br>";
echo Form::submit(null, 'Try it out!', array(
  'class' => 'btn btn-primary'
));
echo Form::close();
