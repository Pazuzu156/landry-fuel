<?php

/**
 * This isn't so much a view as just a PHP script
 * This takes navlinks obtained from the database
 * within the controller, and generates the links
 * in the navbar accordingly. Allows for normal links,
 * dropdowns (based on parent/child relationships), and
 * for dividers and dropdown headers (dividers with content according to db)
 * 
 * Also with support for opening links in new tabs if required (denoted in db)
 */

// Loop through all navlinks (Used as parents/normal links)
for($i = 1; $i <= count($navlinks); $i++)
{
	$link = $navlinks[$i];

	// Normal links
	// Denoted as a links with no children or parents
	if(!$link->has_child
		&& $link->parent == 0)
	{
		$newtab = ($link->newtab) ? array('target' => '_blank') : false;
		echo "<li>" . Html::anchor($link->url, $link->content, $newtab) . "</li>";
	}

	// Parent links
	elseif($link->has_child
		&& $link->parent == 0)
	{
		echo '<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">'
			. $link->content . ' <span class="caret"></span></a>'
			. "<ul class=\"dropdown-menu\">";

		// Loop through all links again, but this time, they're children
		for($c = 1; $c <= count($navlinks); $c++)
		{
			$child = $navlinks[$c];

			// Children must have a parent's id
			// Make sure the child to render matches the
			// parent's id with it's supplied parent id
			// This also is a link, not a divider!
			if($child->parent == $link->id
				&& !$child->divider)
			{
				$newtabc = ($child->newtab) ? array('target' => '_blank') : false;

				// special links (mailto)
				if(startsWith('mailto:', $child->url))
					echo "<li><a href=\"{$child->url}\">{$child->content}</a></li>";
				else
					echo "<li>" . Html::anchor($child->url, $child->content, $newtabc) . "</li>";
			}

			// Same as if statement above, only this time
			// we'll render out the dividers. Both divider
			// and header.
			// Difference denoted by whether or not divider
			// has content (Text to display)
			elseif($child->divider
				&& $child->parent == $link->id)
			{
				if(empty($child->content))
				{
					echo "<li role=\"presentation\" class=\"divider\"></li>";
				}
				else
				{
					echo "<li role=\"presentation\" class=\"dropdown-header\">{$child->content}</li>";
				}
			}
		}
		echo "</ul></li>";
	}
}

function startsWith($needle, $haystack)
{
	return (substr($haystack, 0, strlen($needle)) === $needle);
}