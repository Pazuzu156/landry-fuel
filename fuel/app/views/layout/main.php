<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no">
	<title><?php echo $title; ?></title>
	<?php

	foreach($assets as $key => $value)
	{
		switch($key)
		{
			case 'css':
				foreach($value as $asset)
				{
					echo Asset::css($asset);
				}
				break;
			case 'js':
				foreach($value as $asset)
				{
					echo Asset::js($asset);
				}
				break;
		}
	}

	?>
</head>
<body>
	<nav class="navbar navbar-inverse navbar-static-top" role="navigation">
		<div class="navbar-inner">
			<div class="container">
				<!-- <a class="navbar-brand" href="/~rlandry">Mr. Landry's Website</a> -->
				<?php echo Html::anchor('/', $title, array('class' => 'navbar-brand')); ?>
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Mobile Menu</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
					<ul class="nav navbar-nav navbar-right">
						<?php echo $navbar; ?>
					</ul>
				</nav>
			</div>
		</div>
	</nav>
	<div class="pageContent">
		<div class="container">
			<?php echo $view; ?>
		</div>
	</div>
	<div class="footer">
		<div class="container">
			<div class="footer_text">
				Site &copy; <span class="date"></span> <a href="http://kalebklein.com" target="_blank">Kaleb Klein</a>. | <a href="http://validator.w3.org/check?uri=referer" target="_blank">Valid HTML5</a> | Templating Engine v1.0
			</div>
		</div>
	</div>
</body>
</html>