<?php
/**
 * The development database settings. These get merged with the global settings.
 */

return array(
	// for live testing
	// 'default' => array(
	// 	'type' => 'pdo',
	// 	'connection'  => array(
	// 		'dsn'        => 'mysql:host=162.210.103.230;dbname=admin_landry',
	// 		'username'   => 'admin_landry',
	// 		'password'   => 'Y1ZuFZpLBomHPzbo', // will be changed on production
	// 	),
	// ),

	// for local testing
	'default' => array(
		'type' => 'pdo',
		'connection'  => array(
			'dsn'        => 'mysql:host=localhost;dbname=landry',
			'username'   => 'root',
			'password'   => '', // will be changed on production
		),
	),
	// For sqlite
	// 'default' => array(
	// 	'type' => 'sqlite',
	// 	'connection'  => array(
	// 		'dsn'        => 'sqlite:'.APPPATH.'tmp/landry.sqlite3',
	// 		'persistent'   => false,
	// 		'compress'   => false,
	// 	),
	// 	'identifier' => '"',
	// 	'table_prefix' => '',
	// 	'enable_cache' => true,
	// 	'profiling' => true,
	// 	'charset' => '',
	// ),
);
