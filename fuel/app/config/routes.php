<?php
return array(
	'_root_' => 'home/index',
	'_404_' => 'home/404',
	'testpage(/:presenter)?' => array('home/presenter', 'name' => 'default'),

	// admin routes
	'admin' => 'admin/index',
	'admin/login' => 'admin/get_login',
);
