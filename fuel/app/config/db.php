<?php
/**
 * Use this file to override global defaults.
 *
 * See the individual environment DB configs for specific config information.
 */

return array(
	// 'production' => array(
	// 	'type' => 'pdo',
	// 	'connection' => array(
	// 		'dsn' => 'mysql:host=localhost;dbname=landry',
	// 		'username' => 'root',
	// 		'password' => '',
	// 		'persistent' => false,
	// 		'compress' => false
	// 	),
	// 	'identifier' => '"',
	// 	'table_prefix' => '',
	// 	'charset' => 'utf8',
	// 	'enable_cache' => true,
	// 	'profiling' => false,
	// 	'readonly' => array()
	// )
	'production' => array(
		'type' => 'sqlite',
		'connection'  => array(
			'dsn'        => 'sqlite:'.APPPATH.'tmp/landry.sqlite3',
			'persistent'   => false,
			'compress'   => false,
		),
		'identifier' => '"',
		'table_prefix' => '',
		'enable_cache' => true,
		'profiling' => true,
		'charset' => '',
	),
);
