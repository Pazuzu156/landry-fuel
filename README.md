# Website
The site is powered by [FuelPHP](http://fuelphp.com/).

## Current State
Nothing works outside of the home page and the presenter's page

## Presenters
FuelPHP comes with a "Presenters" workflow allowing you to render data easily from custom URLs. While the process is clean and straightforward, it does not serve the purpose of the site itself (for now.)

Presenters work as normal, however, extending a controller from the custom base controller overrides how presenters are rendered.

For example, presenters are now nothing more than abstract views to be included in the loaded base view.

```php
<?php
...
public function action_presenter()
{
  return Response::forge($this->presenter('home/presenters/form', 'home/form'));
}
..
```

The presenter method takes 3 parameters:  
1. `$presenter` - The abstract presenter view to render  
2. `$baseView` - The base view that loads in the presenter view  
3. `$options` [optional] - Extra options to pass into the base view

When you need to display the renered presenter view, run a check to see if it's got rendered content:  
```php
<?php if(!$abstract->empty); ?>
...
<?php endif; ?>
```

Presenters are loaded into views using the `$abstract` variable. An empty variable means no presenter is rendered, and thus no need to render extra view content. With the presenter variable always there (if you loaded the view using the base controller's `presenter()` method) you can safely call the `$abstract` variable, even if no content is being rendered
