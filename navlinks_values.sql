-- This SQL script is temporary
-- It's used to add values into navlinks table
-- since either FuelPHP has no migration seeder, or I haven't discovered it yet.
-- run: php oil r migrate
-- then upload or paste this SQL code into phpMyAdmin under `dbname`.`navlinks_table` to add values

-- Overview of fields
--
-- id           - The row's ID
-- parent       - The row's parent ID if it's a child link
-- has_child    - If a link should have child links
-- divider      - If a link is a divider (header if divider has content)
-- new_tab      - If the link should open in a new tab
-- content      - The link's content (the text to display)
-- url          - The link's URL
-- created_at   - When the row was created
-- updated_at   - When the row was updated
--

INSERT INTO `navlinks_table` (`id`, `parent`, `has_child`, `divider`, `newtab`, `content`, `url`, `created_at`, `updated_at`) VALUES
--  id      parent  has_child   divider     new_tab     content                             url                                         created_at              updated_at
    (1,     0,      0,          0,          0,          'Home',                             '/',                                        '2016-09-18 00:00:00', '2016-09-18 00:00:00'),
    (2,     0,      0,          0,          0,          'Presenters',                       'testpage',                                 '2016-09-18 00:00:00', '2016-09-18 00:00:00'),
    (3,     0,      1,          0,          0,          'Classes',                          '#',                                        '2016-09-18 00:00:00', '2016-09-18 00:00:00'),
    (4,     0,      1,          0,          0,          'Other Links',                      '#',                                        '2016-09-18 00:00:00', '2016-09-18 00:00:00'),
    (5,     3,      0,          1,          0,          'Spring \'15 Courses',              '',                                         '2016-09-18 00:00:00', '2016-09-18 00:00:00'),
    (6,     3,      0,          0,          0,          'IBT',                              '#',                                        '2016-09-18 00:00:00', '2016-09-18 00:00:00'),
    (7,     3,      0,          0,          0,          'Economics',                        '#',                                        '2016-09-18 00:00:00', '2016-09-18 00:00:00'),
    (8,     3,      0,          0,          0,          'AP Macroeconomics A/B',            '#',                                        '2016-09-18 00:00:00', '2016-09-18 00:00:00'),
    (9,     3,      0,          1,          0,          '',                                 '',                                         '2016-09-18 00:00:00', '2016-09-18 00:00:00'),
    (10,    3,      0,          1,          0,          'Old Courses',                      '',                                         '2016-09-18 00:00:00', '2016-09-18 00:00:00'),
    (11,    3,      0,          0,          0,          'Accounting I',                     '#',                                        '2016-09-18 00:00:00', '2016-09-18 00:00:00'),
    (12,    3,      0,          0,          0,          'Accounting II',                    '#',                                        '2016-09-18 00:00:00', '2016-09-18 00:00:00'),
    (13,    3,      0,          0,          0,          'Computing in the Modern World',    '#',                                        '2016-09-18 00:00:00', '2016-09-18 00:00:00'),
    (14,    3,      0,          0,          0,          'Web Design',                       '#',                                        '2016-09-18 00:00:00', '2016-09-18 00:00:00'),
    (15,    3,      0,          0,          0,          'Advanced Web Design',              '#',                                        '2016-09-18 00:00:00', '2016-09-18 00:00:00'),
    (16,    4,      0,          0,          1,          'FBLA',                             'http://ochsfbla.org/',                     '2016-09-18 00:00:00', '2016-09-18 00:00:00'),
    (17,    4,      0,          0,          1,          'E-Mail Me',                        'mailto:rlandry@oconeeschools.org',         '2016-09-18 00:00:00', '2016-09-18 00:00:00'),
    (18,    4,      0,          0,          1,          'PowerSchool',                      'http://ps.oconee.k12.ga.us/public/',       '2016-09-18 00:00:00', '2016-09-18 00:00:00'),
    (19,    4,      0,          0,          1,          'OCHS Home Page',                   'http://www.oconeeschools.org/domain/8',    '2016-09-18 00:00:00', '2016-09-18 00:00:00');